<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Exceptions\Handler;

class ViewsController extends Controller{
    
    public Function viewRegistar(){ 
        return view('interfaces.registro');    
    }

     public Function viewLogin(){ 
         return view('interfaces.login');     
    }

    public function wieWUdate(){
        return view('interfaces.actualizar');
    }

    public function viewTabla(){
        return view('interfaces.tabla');
        
    }
    public function alertas(){
        return view('interfaces.alertas');
        
    }
    

}