<?php

namespace App\Http\Controllers;


use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Exceptions\Handler;

class UserController extends Controller{

    function registrarse(){
        $user = User::create(request(['name',"email",'password']));
        Crypt::encrypt('password');
        auth()->login($user);
       return view('interfaces.login');
    }

    public function incioLogin(Request $request){
        $consulta = User::where('email',trim($request->email))->first();

        if (!empty($consulta)) {
            if($consulta->password == $request->password){

                session()->put('usuario',$request->email);
                return redirect ( '/tabla' );
            }else{
                return back()->withErrors(['message'=>'error enmail ']);
            }
        }
    }


    public function update($id){
        try {
            $consulta = User::where('id',trim($id))->first();  

            return view('interfaces.actualizar')->with('data',$consulta);
        } catch (Exception $e) {
            return view('alertas');
      }
        
    }

    function update2(Request $request){
        
        //dd($request->all());
        $consulta2 = User::where('id',trim($request->id))->first();
        $consulta2->name=$request->name;
        $consulta2->email=$request->email;
        $consulta2->password=$request->password;
        $consulta2->save(); 

        return redirect ( '/tabla' );
        
    }

    public function mostrar(){
        $consulUsuarios = User::all();

        if(!empty($consulUsuarios)){
            
            $datosUsuarios = []; 
            /*foreach ($consulUsuarios as $usuario) {
                $array = array(
                'id'=>$usuario->id,
                'nombre'=>$usuario->name,
                'email'=>$usuario->email,
                'contreseña'=>Crypt::decrypt($usuario->password));
                $datosUsuarios[]= $array;
            }*/
            return view('interfaces.tabla')->with('data',$consulUsuarios);  
        }else{
            return response()->json(['satus'=>401]);
        }
        
    }


    public function delete($id){
        try {
            
            $userDelete =User::where('id',$id)->delete();       
            return redirect('/tabla');
        } catch (Exception $e) {
            return response()->json(['data'=>'No se elimino'], 401);
        }

    }

    public function destroySession(){
        session()->flush();
        return redirect('/login');
    }

   

}
