<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>@yield('title') - Laravel APP</title>
    
    <link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/tailwindcss/2.0.1/tailwind.min.css">
  </head> 

  <body class='bg-gray-300' >
    <nav class="flex py-5 bg-blue-500  ">
      <div class="w-1/2 px-12 mr-auto" >
        <a href="{{ route('login') }}" ><p class="text-3xl font-bold "> CIDENET APP</p></a>
      </div>

     <ul class=' w-1/2 px-16 ml-auto flex justify-end pt-1'>
        <li class="mx-7">
          <a href="{{ route('login') }}" class="font-semibold 
          hover:bg-blue-50 py-2 px-4 rounded-md  border-2 border-white  ">Inciar Sesión</a>
        </li>
        <li>
        <a href="{{ route('registro.index') }}"class="font-semibold 
        border-2 borde-black py-2 px-4 rounded-md hover:bg-blue-50">Registrarse</a>
        </li>   
    
    </ul>
   
    </nav>
    
    

    @yield('content')
  </body>
</html>