
@extends('layouts.app')

@section('title','tabla')
@section('content')


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://kit.fontawesome.com/a23e6feb03.js"></script>
<div class="bg-white overflow-hidden shadow-xl sm:rounded-lg ">
    <h1 class="text-3xl text-center pt=24 font-bold bg-blue-200" > Listado de Usuarios</h1>
    

    <table class="table-fixed w-11/12 mx-10 my-5 bg-green-100">
      <thead>
        <tr class="bg-green-300 text-black">
          <th class="w-20 py-4 ...">ID</th>
          <th class="w-1/2 py-4 text-left ...">Nombre</th>
          <th class="w-1/16 py-4 ...">Email</th>
          <th class="w-1/16 py-4 ...">Password</th>
          <th class="w-28 py-4 ..."></th>
        </tr>
      </thead>
      <tbody>
        @foreach ($data as $user)
              <tr>
              <td class="py-3 px-6"> {{$user->id}}</td>
              <td class="p-3">{{$user->name}}</td>
              <td class="p-3 text-center">{{$user->email}}</td>
              <td class="p-3 text-center">{{$user->password}}</td>
              <td class="p-3">
                <a href="{{route('delete',array('id'=>$user->id)) }}"><button 
                  class="bg-red-500 text-white px-3 py-1 rounded-sm">
                  <i class="fas fa-trash"></i></button>

                <a href="{{route('update.index',array('id'=>$user->id)) }}"> <button 
                  class="bg-green-500 text-white px-3 py-1 rounded-sm">
                  <i class="fas fa-pen"></i></button></a>
              </td>
              </tr>
        @endforeach
        

      </tbody>
    </table>
</div>

@endsection