@extends('layouts.app')


@section('title','registro')
@section('content')
<div class="block mx-auto my-20 p-20 w-1/3 bg-gray-50 border gray-200 rounded-lg shadow bg-blue-100">
    <h1 class="text-3xl text-center pt=24 font-bold bg-blue-200"> Registrarse</h1>

    <form class="mt-4 " method="POST" action="">
        @csrf
        <input type="text" class="rouded-md bg-blue-100 text-lg 
            placeholder-gray-900 p-2 my-2 focus:bg-white w-full" placeholder="Nombre"
        id="namw" name="name" required >
        <input type="email" class="rouded-md bg-blue-100 text-lg 
            placeholder-gray-900 p-2 my-2 focus:bg-white w-full" placeholder="Email"
        id="email" name="email" required>
        <input type="password" class="border-gray-200 rouded-md bg-blue-100 text-lg
            placeholder-gray-900 p-2 my-2 focus:bg-white w-full"placeholder="Contraseña"
        id="password" name="password" required>
        

        <button type="submit" class="rounded-md bg-blue-400 w-full text.lg text-black 
        font-semibold p-2 n-3 hover:bg-indigo-600" >Enviar</button>

    </form>
</div> 
@endsection
    