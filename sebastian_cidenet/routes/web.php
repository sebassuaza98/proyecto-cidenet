<?php

use App\Http\Controllers\ViewsController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;


Route::get('/',function(){
   return view('interfaces.login');
});

Route:: get('login',[ViewsController::class,'viewLogin'])->name('login');
Route:: post('login',[UserController::class,'incioLogin'])->name('incioLogin.index');

Route:: get('registro',[ViewsController::class,'viewRegistar'])->name('registro.index');
Route:: post('registro',[UserController::class,'registrarse'])->name('registroLogeo');

Route:: get('tabla',[UserController::class,'mostrar'])->name('viewTabla.index')->middleware('auth');

Route:: get('actualizar/{id}',[UserController::class,'update'])->name('update.index');
Route:: post('actualizar',[UserController::class,'update2'])->name('update2.index');

Route:: get('alertas',[ViewsController::class,'alertas'])->name('alert');

Route:: get('/loyout',[UserController::class,'destroySession'])->name('destroySession.index');

Route:: get('delete/{id}',[UserController::class,'delete'])->name('delete');
